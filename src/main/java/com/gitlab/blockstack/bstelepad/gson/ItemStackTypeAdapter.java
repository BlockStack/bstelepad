package com.gitlab.blockstack.bstelepad.gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

public class ItemStackTypeAdapter extends TypeAdapter<ItemStack> {

	@Override
	public ItemStack read(JsonReader reader) throws IOException {
		String material = null;
		String count = null;
		String name = null;
		List<String> lore = new ArrayList<>();
		String key;
		reader.beginObject();
		while (reader.hasNext()) {
			key = reader.nextName();
			switch (key) {
			case "material":
				material = reader.nextString();
				break;
			case "count":
				count = reader.nextString();
				break;
			case "meta_name":
				name = reader.nextString();
				break;
			case "lore":
				reader.beginArray();
				while(reader.peek() == JsonToken.STRING) {
					lore.add(reader.nextString());
				}
				reader.endArray();
				break;
			case "empty":
				reader.nextString();
				reader.endObject();
				return null;
			}

		}
		if(material == null) {
			return null;
		}
		reader.endObject();
		ItemStack item = new ItemStack(Material.valueOf(material));
		item.setAmount(Integer.parseInt(count));
		ItemMeta meta = item.getItemMeta();
		if(name != null) {
			meta.setDisplayName(name);
		}
		if(!lore.isEmpty()) {
			meta.setLore(lore);
		}
		item.setItemMeta(meta);
		return item;
	}

	@Override
	public void write(JsonWriter writer, ItemStack item) throws IOException {
		if(item==null) {
			writer.beginObject();
			writer.name("empty").value("empty");
			writer.endObject();
			return;
		}
		writer.beginObject();
		writer.name("material").value(item.getType().toString());
		writer.name("count").value(item.getAmount());
		if(item.hasItemMeta()) {
			ItemMeta meta = item.getItemMeta();
			writer.name("meta_name").value(meta.getDisplayName());
			writer.name("lore").beginArray();
			for(String str : meta.getLore()) {
				writer.value(str);
			}
			writer.endArray();
		}
		writer.endObject();
	}

}
