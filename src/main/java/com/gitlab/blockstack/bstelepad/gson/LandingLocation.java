package com.gitlab.blockstack.bstelepad.gson;

import java.util.Optional;

import com.gitlab.blockstack.bstelepad.BSTelepad;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * Safe basic representation of a location that be written by GSON.
 */
public class LandingLocation {
    private String world = null;
	private double x = 0, y = 0, z = 0;
	private float pitch = 0, yaw = 0;

    /**
     * Constructor
     * @param world The name of the world.
     * @param x The X position.
     * @param y The y position.
     * @param z The z position.
     * @param pitch The pitch.
     * @param yaw The yaw.
     */
    public LandingLocation(String world, double x, double y, double z, float pitch, float yaw) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
    }

    /**
     * Create a new LandingLocation from a bukkit location.
     * @param location Bukkit location.
     * @return LandingLocation equivelent to the bukkit location.
     */
    public static LandingLocation fromLocation(Location location) {
        return new LandingLocation(location.getWorld().getName(), location.getX(), location.getY(), location.getZ(), location.getPitch(), location.getYaw());
    }

    /**
     * Optionally return the bukkit location equivelent of this location.
     * @return The bukkit location of this location or empty if the world could not be loaded.
     */
    public Optional<Location> toLocation() {
        World worldObj = Bukkit.getWorld(world);
        if(worldObj == null) {
            BSTelepad.self().getLogger().warning("Telepad could not find the world! :: "+ world);
            return Optional.empty(); 
        }
        return Optional.of(new Location(worldObj, x, y, z, yaw, pitch));
    }
    
    
}
