package com.gitlab.blockstack.bstelepad.listeners;

import com.gitlab.blockstack.bstelepad.tasks.TeleportTask;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class EventListener implements Listener {
    
    @EventHandler
    public void acceptTelepad(PlayerToggleSneakEvent event) {
        if(TeleportTask.runningTasks.containsKey(event.getPlayer())) {
            TeleportTask.runningTasks.get(event.getPlayer()).sneaked();
        }
    }
}