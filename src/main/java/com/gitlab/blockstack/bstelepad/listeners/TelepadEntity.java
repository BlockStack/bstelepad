package com.gitlab.blockstack.bstelepad.listeners;

import java.util.Arrays;
import java.util.Map.Entry;
import java.util.Optional;

import com.gitlab.blockstack.bstelepad.BSTelepad;
import com.gitlab.blockstack.bstelepad.Config;
import com.gitlab.blockstack.bstelepad.exceptions.TeleportRunningException;
import com.gitlab.blockstack.bstelepad.persistence.v1.ItemStackWrapper;
import com.gitlab.blockstack.bstelepad.persistence.v1.TelepadData;
import com.gitlab.blockstack.bstelepad.tasks.TeleportTask;
import com.planarshiftmc.api.ui.AbstractUI;
import com.planarshiftmc.api.ui.builder.UiBuilder;
import com.planarshiftmc.api.ui.builder.UiItemBuilder;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import de.Ste3et_C0st.FurnitureLib.main.Furniture;
import de.Ste3et_C0st.FurnitureLib.main.ObjectID;
import de.Ste3et_C0st.FurnitureLib.main.Type.SQLAction;

public class TelepadEntity extends Furniture implements Listener {

    protected TelepadData telepadData;
    protected boolean guiopen = false;

    public TelepadEntity(ObjectID id) {
        super(id);
        TelepadData data = BSTelepad.self().getTelepads().get(id.getID());
        if(data == null) {
            if(id.getStartLocation() == null) {
                BSTelepad.self().getLogger().warning("Start location is null!");
            }
            data = new TelepadData(id.getID(), id.getStartLocation(), id.getPlayerList().stream().findFirst().get().getUniqueId());
        }
        BSTelepad.self().updateTelepad(data, true);
        this.telepadData = data;
        Bukkit.getPluginManager().registerEvents(this, BSTelepad.self());
    }

    public TelepadEntity(ObjectID id, TelepadData data) {
        super(id);
        this.telepadData = data;
        Bukkit.getPluginManager().registerEvents(this, BSTelepad.self());
    }

    @Override
    public void spawn(Location loc) {
        //Spawn is handled elsewhere we need to keep this from doing anything
    }

    // Handles furniture click
    @Override
    public void onClick(Player player) {
        if (getObjID() == null)
			return;
		if (getObjID().getSQLAction().equals(SQLAction.REMOVE))
			return;
		if (player == null)
			return;
        if (!canBuild(player)) {
            return;
        }
        synchronized(this) {
            if(this.guiopen) {
                return;
            }
            if (this.telepadData.getOwner().equals(player.getUniqueId()) || player.hasPermission(Config.PERMS.ADMIN)) {
                this.editGui(player).open();
                this.guiopen=true;
            }
        }
    }

    @EventHandler
    public void onPlayerWalk(PlayerMoveEvent event) {
        // make sure we have moved some distance
        if (event.getFrom().getBlock().getLocation().equals(event.getTo().getBlock().getLocation())) {
            return;
        }
        //old ones that didn't save correctly end up with null data
        if(this.telepadData == null) {
            return;
        }
        //Did the step on this telepad
        if (event.getTo().getBlock().getLocation().equals(this.getLocation().getBlock().getLocation())) {
            //check the cache
            if(!this.telepadData.destinationCache.isPresent()) {
                //Stream that find matching address for the destination
                Optional<Entry<String,TelepadData>> telepad = BSTelepad.self().getTelepads().entrySet().stream()
                    .filter((Entry<String,TelepadData> ent)->{
                        return !this.telepadData.getId().equals(ent.getValue().getId());
                    })
                    .filter((Entry<String,TelepadData> ent)->{
                        //check only enabled telepads
                        return ent.getValue().enabled;
                    })
                    .filter((Entry<String,TelepadData> ent)->{
                        //check for matching telepad
                        return Arrays.hashCode(ent.getValue().address) == Arrays.hashCode(this.telepadData.destination);
                }).findFirst();
                if(telepad.isPresent()) {
                    this.telepadData.destinationCache = Optional.of(telepad.get().getValue().getId());
                }
            }
        
            if(this.telepadData.destinationCache.isPresent()) {
                try {
                    TelepadData destination = BSTelepad.self().getTelepads().get(this.telepadData.destinationCache.get());

                    //Determine cost
                    Double cost = destination.calculateCost(event.getPlayer().getLocation());
                    if(event.getPlayer().hasPermission(Config.PERMS.ADMIN)) {
                        cost = 0d;
                    }
                    TeleportTask task = new TeleportTask(event.getPlayer(),destination, cost);
                    if(cost > 0) {
                        event.getPlayer().sendTitle("Sneak to teleport.", "This will cost $"+cost, 5, 40, 5);
                    } else {
                        event.getPlayer().sendTitle("Sneak to teleport.", "", 5, 40, 5);
                    }
                    Bukkit.getScheduler().runTaskLater(BSTelepad.self(), task, Config.TELEPAD_WAIT_TIME);
                } catch (TeleportRunningException te) {
                    //no need to sat anything
                }
            } else {
                event.getPlayer().sendMessage(ChatColor.RED + "Destination does not match any telepads!" + ChatColor.RESET);
                return;
            }
        }
    }

    @Override
    public void onBreak(Player player) {
        if (getObjID() == null)
			return;
		if (getObjID().getSQLAction().equals(SQLAction.REMOVE))
			return;
		if (player == null)
			return;
        if (!canBuild(player)) {
            return;
        }
        if(this.telepadData != null && !this.telepadData.isEmpty()) {
            player.sendMessage(ChatColor.RED+"Telepad cannot be broken until empty!"+ChatColor.RESET);
        } else {
            BSTelepad.self().removeTelepad(this.telepadData);
            this.destroy(player);
            HandlerList.unregisterAll(this);
        }
    }

    protected AbstractUI editGui(Player player) {
        UiBuilder builder = UiBuilder.newInstance("Edit Telepad", BSTelepad.self(), player);
        builder.setAddAcceptCancelButtons(false);
        // Address
        builder.addInventoryElement(UiItemBuilder.newInstance("Address #1", "First address key block.").isItemStack()
                .setItemStack(this.telepadData.address[0].getItemStack()).build());
        builder.addInventoryElement(UiItemBuilder.newInstance("Address #2", "Second address key block.").isItemStack()
                .setItemStack(this.telepadData.address[1].getItemStack()).build());
        builder.addInventoryElement(UiItemBuilder.newInstance("Address #3", "Third address key block.").isItemStack()
                .setItemStack(this.telepadData.address[2].getItemStack()).build());
        // Filler
        builder.addInventoryElement(UiItemBuilder.newInstance("<--Address", "").isFiller().build());
        builder.addInventoryElement(UiItemBuilder.newInstance("", "").isFiller().build());
        builder.addInventoryElement(UiItemBuilder.newInstance("Destionation-->", "").isFiller().build());
        // Destination
        builder.addInventoryElement(UiItemBuilder.newInstance("Destination #1", "First destination key block.")
                .setItemStack(this.telepadData.destination[0].getItemStack()).isItemStack().build());
        builder.addInventoryElement(UiItemBuilder.newInstance("Destination #2", "Second destination key block.")
                .setItemStack(this.telepadData.destination[1].getItemStack()).isItemStack().build());
        builder.addInventoryElement(UiItemBuilder.newInstance("Destination #3", "Third destination key block.")
                .setItemStack(this.telepadData.destination[2].getItemStack()).isItemStack().build());

        builder.setCallback((pl, status, answers) -> {
            this.telepadData.address[0] = new ItemStackWrapper((ItemStack) answers.get("Address #1").getAnswer().get());
            this.telepadData.address[1] = new ItemStackWrapper((ItemStack) answers.get("Address #2").getAnswer().get());
            this.telepadData.address[2] = new ItemStackWrapper((ItemStack) answers.get("Address #3").getAnswer().get());

            this.telepadData.destination[0] = new ItemStackWrapper((ItemStack) answers.get("Destination #1").getAnswer().get());
            this.telepadData.destination[1] = new ItemStackWrapper((ItemStack) answers.get("Destination #2").getAnswer().get());
            this.telepadData.destination[2] = new ItemStackWrapper((ItemStack) answers.get("Destination #3").getAnswer().get());

            // run the save out of main thread
            Bukkit.getScheduler().runTaskAsynchronously(BSTelepad.self(), () -> {
                // clear the cache as the update may have changed the destination
                this.telepadData.destinationCache = Optional.empty();
                // enable this telepad
                this.telepadData.enabled = true;
                //make sure the address is set.
                if(this.telepadData.address[0].isAir() && 
                    this.telepadData.address[1].isAir() && 
                    this.telepadData.address[2].isAir()) {
                        pl.sendMessage(ChatColor.RED + "The telepad must have an address set." + ChatColor.RESET);
                        this.telepadData.enabled = false;
                }
                //check for address uniqueness.
                if(this.telepadData.enabled) {
                    boolean notUnique = BSTelepad.self().getTelepads().entrySet().stream()
                        .filter((Entry<String,TelepadData> ent)->{
                            return !this.telepadData.getId().equals(ent.getValue().getId());
                        })
                        .filter((Entry<String,TelepadData> ent)->{
                            //only check against enabled telepads
                            return ent.getValue().enabled;
                        }).anyMatch((Entry<String,TelepadData> ent) -> {
                            //check that address is unique
                            return Arrays.hashCode(this.telepadData.address) == Arrays.hashCode(ent.getValue().address);
                        });
                    if(notUnique) {
                        pl.sendMessage("The telepad address must be unique.  Please change it.");
                        this.telepadData.enabled = false;
                    }
                }
                BSTelepad.self().updateTelepad(this.telepadData,true);
                this.guiopen = false;
            });
        });
        return builder.buildInventoryUI();
    }
}