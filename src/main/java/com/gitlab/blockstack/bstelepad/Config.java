package com.gitlab.blockstack.bstelepad;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.plugin.Plugin;

/**
 * Central Configuration handler.
 * 
 * @author wispoffates
 */
public class Config {
    private static Integer TICKS_PER_SECOND = 20;
    /**
     * The storage type used for player data.
     */
    public static STORAGEMODE STORAGE_TYPE = STORAGEMODE.FILES;
    public static Boolean TELEPAD_DEFAULT_COST_BASED_ON_DISTANCE = false;
    public static Double TELEPAD_TP_COST = .0001;
    public static Integer TELEPAD_WAIT_TIME = 5 *TICKS_PER_SECOND; //5 seconds
    public static Double TELEPAD_MIN_COST = 0d;
    public static Double TELEPAD_MAX_COST = 400d;

    public Config(Plugin plugin) {
        STORAGE_TYPE = STORAGEMODE.fromString(plugin.getConfig().getString("storageType", STORAGE_TYPE.name()));
        TELEPAD_DEFAULT_COST_BASED_ON_DISTANCE = plugin.getConfig().getBoolean("Telepad.DefaultCostBasedOnDistance",TELEPAD_DEFAULT_COST_BASED_ON_DISTANCE);
        TELEPAD_TP_COST = plugin.getConfig().getDouble("Telepad.TPCost",TELEPAD_TP_COST);
        TELEPAD_WAIT_TIME = this.strToTicks(plugin.getConfig().getString("Telepad.WaitTime"), TELEPAD_WAIT_TIME);
        TELEPAD_MAX_COST = plugin.getConfig().getDouble("Telepad.MaxCost",TELEPAD_MAX_COST);
        TELEPAD_MIN_COST = plugin.getConfig().getDouble("Telepad.MinCost",TELEPAD_MIN_COST);
    }

    public static String[] debug() {
        List<String> vals = new LinkedList<>();
        vals.add("storageType="+ STORAGE_TYPE);
        vals.add("Telepad.DefaultCostBasedOnDistance="+ TELEPAD_DEFAULT_COST_BASED_ON_DISTANCE);
        vals.add("Telepad.TPCost="+ TELEPAD_TP_COST);
        return vals.toArray(new String[0]);
    }

    private Integer strToTicks(String in, Integer def) {
        if (in == null || in.isEmpty()) {
            return def;
        }
        try {
            if (in.contains("t")) {
                return Integer.parseInt(in.replace("t", ""));
            }
            if (in.contains("s")) {
                return Integer.parseInt(in.replace("s", "")) * TICKS_PER_SECOND;
            }
            if (in.contains("m")) {
                return Integer.parseInt(in.replace("m", "")) * TICKS_PER_SECOND * 60;
            }
            if (in.contains("h")) {
                return Integer.parseInt(in.replace("h", "")) * TICKS_PER_SECOND * 60 * 60;
            }
            if (in.contains("d")) {
                return Integer.parseInt(in.replace("d", "")) * TICKS_PER_SECOND * 60 * 60 * 24;
            }
            return def;
        } catch (Exception e) {
            BSTelepad.self().getLogger().warning("Input could not be convertred to ticks! Using default. value=" + in);
            return def;
        }
    }

    /**
     * Holds the perms for the plugin.
     */
    public static class PERMS {
        public static final String ADMIN = "telepad.admin";
        public static final String COMMAND = "telepad.command";
    }

    /**
     * Controls the way player data is stored. Currently only supports files. FILES
     * - Stores playerdata in JSON using GSON.
     */
    public static enum STORAGEMODE {
        FILES;

        public static STORAGEMODE fromString(String in) {
            if (in.trim().equalsIgnoreCase("FILES")) {
                return STORAGEMODE.FILES;
            } else {
                BSTelepad.self().getLogger()
                        .warning("STORAGE MODE returned default FILES.  Receieved invalid value: " + in);
                return STORAGEMODE.FILES;
            }
        }
    }
}