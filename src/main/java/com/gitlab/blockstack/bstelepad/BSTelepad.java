package com.gitlab.blockstack.bstelepad;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;

import com.gitlab.blockstack.TelepadFurnitureHook;
import com.gitlab.blockstack.bstelepad.commands.AdminCommandHandler;
import com.gitlab.blockstack.bstelepad.commands.CommandHandler;
import com.gitlab.blockstack.bstelepad.gson.ItemStackTypeAdapter;
import com.gitlab.blockstack.bstelepad.listeners.EventListener;
import com.gitlab.blockstack.bstelepad.persistence.v1.TelepadData;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import de.Ste3et_C0st.FurnitureLib.main.FurniturePlugin;
import net.milkbowl.vault.economy.Economy;

/**
 * BSTelepad Plugin
 *
 * @author ${author}
 */
public class BSTelepad extends JavaPlugin {
	private static BSTelepad self;
	private static Economy econ;
	protected static Map<String, TelepadData> telepads = new HashMap<>();

	/**
	 * Retrieve the plugin instance.
	 * 
	 * @return The SpigotPlugin instance.
	 */
	public static BSTelepad self() {
		return self;
	}

	@Override
	public void onEnable() {
		getCommand("BSTelepad").setExecutor(new CommandHandler());
		getCommand("BSTelepadadmin").setExecutor(new AdminCommandHandler());
		getServer().getPluginManager().registerEvents(new EventListener(), this);
		self = this;

		// load the config
		File cfgFile = new File(getDataFolder(), "config.yml");
		if (!cfgFile.exists()) {
			this.saveDefaultConfig();
		}
		@SuppressWarnings("unused") // this loads the config
		Config config = new Config(this);

		this.setupEconomy();
		// load telepads
		this.loadTelepads();
		FurniturePlugin telepadHook = new TelepadFurnitureHook(this);
		telepadHook.register();

		this.getLogger().info("BSTelepad enabled.");
	}
	
	
	@Override
	public void onDisable() {
		self = null;
		this.getLogger().info("{BSTelepad} disabled.");
	}

	public Map<String, TelepadData> getTelepads() {
		return telepads;
	}

	/**
	 * Load the telepads.
	 */
	protected void loadTelepads() {
		Gson gson = BSTelepad.getGson(true);
		File configFolder = this.getDataFolder();
		configFolder.mkdirs();
		File telepadsFile = new File(configFolder, "telepads.json");
		if (telepadsFile.exists()) {
			try (FileReader reader = new FileReader(telepadsFile)) {
				Type mapType = new TypeToken<Map<String, TelepadData>>() {
				}.getType();
				BSTelepad.telepads = gson.fromJson(reader, mapType);
			} catch (Exception e) {
				this.getLogger().log(Level.SEVERE, e, () -> {
					return "Error loading telepads!";
				});
				try {
					Files.move(telepadsFile, new File(configFolder, "telepads.damaged"));
				} catch (IOException e1) {
					//Give up...
				}
			}
		}
	}

	/**
	 * Save the telepads to persistent storage.
	 */
	protected void saveTelepads() {
		Gson gson = BSTelepad.getGson(true);
		File configFolder = this.getDataFolder();
		configFolder.mkdirs();
		File telepadsFile = new File(configFolder, "telepads.json");
		try (FileWriter writer = new FileWriter(telepadsFile)) {
			Type mapType = new TypeToken<Map<String, TelepadData>>() {
			}.getType();
			gson.toJson(BSTelepad.telepads,mapType, writer);
			writer.flush();
		} catch (Exception e) {
			this.getLogger().log(Level.SEVERE, e, () -> {
				return "Error writing telepads!";
			});
		}
	}

	/**
	 * Update the provided telepad.
	 * 
	 * @param data The telepad to update.
	 */
	public void updateTelepad(TelepadData data, boolean save) {
		telepads.put(data.getId(), data);
		if(save) {
			this.saveTelepads();
		}
		this.invalidateCaches(data);
	}

	private void invalidateCaches(TelepadData invalidate) {
		for(TelepadData data : telepads.values()) {
			if(data.destinationCache.isPresent() && data.destinationCache.get().equals(invalidate.getId())) {
				data.destinationCache = Optional.empty();
			}
		}
	}
	/**
	 * Remove a telepad.
	 * @param data Telepad to remove.
	 */
	public void removeTelepad(TelepadData data) {
		telepads.remove(data.getId());
		this.saveTelepads();
		this.invalidateCaches(data);
	}

	/**
	 * Retrieve the instance of the economy api.
	 */
	public static Economy getEconomy() {
		return econ;
	}

    /**
	 * Retrieve primed gson instance.
	 * 
	 * @param pretty Enable pretty print.
	 * @return GSON instance.
	 */
	public static Gson getGson(boolean pretty) {
		GsonBuilder builder = new GsonBuilder();
		if (pretty) {
				builder.setPrettyPrinting();
		}
		builder.registerTypeHierarchyAdapter(ItemStack.class, new ItemStackTypeAdapter());
		return builder.create();
	}

	private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

}
