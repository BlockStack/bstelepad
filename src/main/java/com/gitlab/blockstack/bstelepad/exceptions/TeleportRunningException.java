package com.gitlab.blockstack.bstelepad.exceptions;

public class TeleportRunningException extends Exception {

    private static final long serialVersionUID = 1L;

    public TeleportRunningException(String msg) {
        super(msg);
    }
}