package com.gitlab.blockstack.bstelepad.persistence.v1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.google.common.reflect.TypeToken;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.Repairable;
import org.bukkit.inventory.meta.SkullMeta;

/**
 * ItemStackWrapper
 * 
 * Minecraft ItemStacks come with so much extra data that its hard to compare
 * them. Lets wrap it in something more controllable.
 */
public class ItemStackWrapper {
    private Material mat = Material.AIR;
    private Integer amount = 0;
    private Map<String,Object> meta = new HashMap<>();
    
    /**
     * GSON constructor
     */
    protected ItemStackWrapper() {
    
    }

    public ItemStackWrapper(ItemStack item) {
        this.mat = item.getType();
        this.amount = item.getAmount();
        if(item.hasItemMeta()) {
           this.readMeta(item.getItemMeta()); 
        }
    }

    public ItemStack getItemStack() {
        ItemStack stack = new ItemStack(mat,amount);
        stack.setItemMeta(this.setMeta(stack));
        return stack;
    }

    public boolean isAir() {
        return this.mat == Material.AIR;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mat,amount,meta);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        ItemStackWrapper other = (ItemStackWrapper) o;
         return other.mat == this.mat 
            && other.amount.equals(this.amount)
            && other.meta.equals(this.meta);
    }

    private void readMeta(ItemMeta meta) {
        //print out types that we havent implemented
        TypeToken<? extends ItemMeta>.TypeSet set = TypeToken.of(meta.getClass()).getTypes();
        List<String> implemented = Arrays.asList("Repairable","Damageable","ItemMeta");
        set.stream().map((TypeToken<?> type)->{return type.getType().getTypeName();})
        .filter((String name) -> {return name.contains(".meta.");})
        .filter(
            (String name) -> {
                for(String impl : implemented) {
                    if(name.contains(impl)) return false;
                }
                return true;
            }
        ).forEach((String s) -> {System.out.println("[BSTelepad] Unimplemented type: "+s);});
        //end print out
        if(meta.hasDisplayName()) {
            this.meta.put("ItemMeta.displayName", meta.getDisplayName());
        }
        if(meta.hasLore()) {
            this.meta.put("ItemMeta.lore", meta.getLore());
        }
        //TODO: Attribute and Flags

        //Repairable
        if(meta instanceof Repairable) {
            Repairable repairable = (Repairable) meta;
            if(repairable.hasRepairCost()) {
                this.meta.put("Repairable.repairCost", repairable.getRepairCost());
            }
        }
        if(meta instanceof Damageable) {
            Damageable damageable = (Damageable) meta;
            if(damageable.hasDamage()) {
                this.meta.put("Damagable.damage", damageable.getDamage());
            }
        }
        if(meta instanceof SkullMeta) {
            //SkullMeta skullMeta = (SkullMeta) meta;
            //TODO: how to handle this
        }

    }

    private ItemMeta setMeta(ItemStack stack) {
        ItemMeta meta = stack.getItemMeta();
        ItemStackWrapper itemStackWrapper = this;
		if(itemStackWrapper.meta.containsKey("ItemMeta.displayName")) {
            meta.setDisplayName((String) itemStackWrapper.meta.get("ItemMeta.displayName"));
        }
        if(itemStackWrapper.meta.containsKey("ItemMeta.lore")) {
            meta.setLore((List<String>) itemStackWrapper.meta.get("ItemMeta.lore"));
        }
        if(itemStackWrapper.meta.containsKey("Repairable.repairCost")) {
            Repairable repairable = (Repairable) meta;
            repairable.setRepairCost((int) makeInteger(itemStackWrapper.meta.get("Repairable.repairCost")));
        }
        if(itemStackWrapper.meta.containsKey("Damagable.damage")) {
            Damageable damageable = (Damageable) meta;
            damageable.setDamage((int) makeInteger(itemStackWrapper.meta.get("Damagable.damage")));
        }
        return meta;
    }

    private Integer makeInteger(Object o) {
        if(o instanceof Integer) {
            return (Integer) o;
        }
        if(o instanceof Double) {
            return ((Double) o).intValue();
        }
        return null;
    }
}