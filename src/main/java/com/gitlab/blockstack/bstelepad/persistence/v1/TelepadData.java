package com.gitlab.blockstack.bstelepad.persistence.v1;

import java.util.Optional;
import java.util.UUID;

import com.gitlab.blockstack.bstelepad.Config;
import com.gitlab.blockstack.bstelepad.gson.LandingLocation;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.inventory.ItemStack;

/**
 * Holds configuration data of individual telepads
 */
public class TelepadData {
    protected LandingLocation location;
    protected String furnitureId;
    protected UUID owner;
    public boolean enabled = false;
    public ItemStackWrapper[] address = new ItemStackWrapper[3];
    public ItemStackWrapper[] destination = new ItemStackWrapper[3];

    public transient Optional<String> destinationCache = Optional.empty();

    /** GSON Constructor */
    protected TelepadData() {
        this.address[0] = new ItemStackWrapper(new ItemStack(Material.AIR,0));
        this.address[1] = new ItemStackWrapper(new ItemStack(Material.AIR,0));
        this.address[2] = new ItemStackWrapper(new ItemStack(Material.AIR,0));
        this.destination[0] = new ItemStackWrapper(new ItemStack(Material.AIR,0));
        this.destination[1] = new ItemStackWrapper(new ItemStack(Material.AIR,0));
        this.destination[2] = new ItemStackWrapper(new ItemStack(Material.AIR,0));
    }

    /**
     * Construction
     * 
     * @param id       The furniture id
     * @param location Location of the telepad
     */
    public TelepadData(String id, Location location, UUID owner) {
        this();
        this.furnitureId = id;
        this.location = LandingLocation.fromLocation(location);
        this.owner = owner;
    }

    /**
     * Retrieve the location of the telepad
     * 
     * @return The telepad location.
     */
    public Location getLocation() {
        return this.location.toLocation().orElse(null);
    }

    /**
     * Retrieve the furniture id of the telepad.
     * 
     * @return The telepad furniture id.
     */
    public String getId() {
        return this.furnitureId;
    }

    /**
     * Retrieve the owner uuid.
     * 
     * @return The owner id.
     */
    public UUID getOwner() {
        return this.owner;
    }

    /**
     * The player to teleport.
     * @param player The player.
     */
    public Optional<Location> safeTeleport() {
        Block block = this.getLocation().getBlock();
        block.getChunk().load(false);
        return Optional.of(block.getLocation().add(0, 1, 0));
    }

    private Location testLocation(final Location loc) {
        if(loc.getBlock().isEmpty() 
            && loc.getBlock().getRelative(BlockFace.UP).isEmpty()
            && !loc.getBlock().getRelative(BlockFace.DOWN).isEmpty()) {
                return loc.getBlock().getLocation();
        }
        //try up one
        Location up = loc.clone().add(0, 1, 0);
        if(up.getBlock().isEmpty() 
            && up.getBlock().getRelative(BlockFace.UP).isEmpty()
            && !up.getBlock().getRelative(BlockFace.DOWN).isEmpty()) {
                return up.getBlock().getLocation();
        }
        //try down one
        Location down = loc.clone().add(0, -1, 0);
        if(down.getBlock().isEmpty() 
            && down.getBlock().getRelative(BlockFace.UP).isEmpty()
            && !down.getBlock().getRelative(BlockFace.DOWN).isEmpty()) {
                return down.getBlock().getLocation();
        }
        return null;
    }

    /**
     * Calculate the cost to teleport to this pad.
     * @param loc The location teleporting from.
     * @return The calculated cost.
     */
    public Double calculateCost(final Location loc) {
        if(this.address[0] != null && !this.address[0].isAir() && this.address[0].getItemStack().getType() == Material.BEDROCK) {
            return 0d;
        }
        Double cost = Config.TELEPAD_TP_COST;
        //make sure telepads distance measurement occurs only on the same world
        if(!loc.getWorld().equals(this.getLocation().getWorld())) {
            cost = Config.TELEPAD_MAX_COST;
        } else if(Boolean.TRUE.equals(Config.TELEPAD_DEFAULT_COST_BASED_ON_DISTANCE)) {
            Double dist = loc.distance(this.getLocation());
            cost = cost * dist;
        }
        if(cost>Config.TELEPAD_MAX_COST) {
            cost = Config.TELEPAD_MAX_COST;
        }
        if(cost<Config.TELEPAD_MIN_COST) {
            cost = Config.TELEPAD_MIN_COST;
        }
        
        return Math.round(cost * 100.0) / 100.0; //2 decimal places
    }

    /**
     * Checks if the 
     */
    public boolean isEmpty() {
        return(this.address[0].isAir()
            && this.address[1].isAir()
            && this.address[2].isAir()
            && this.destination[0].isAir()
            && this.destination[1].isAir()
            && this.destination[2].isAir());
          
    }
}