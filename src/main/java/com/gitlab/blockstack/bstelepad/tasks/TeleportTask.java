package com.gitlab.blockstack.bstelepad.tasks;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.gitlab.blockstack.bstelepad.BSTelepad;
import com.gitlab.blockstack.bstelepad.exceptions.TeleportRunningException;
import com.gitlab.blockstack.bstelepad.persistence.v1.TelepadData;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import net.milkbowl.vault.economy.EconomyResponse;

/**
 * 
 */
public class TeleportTask implements Runnable {

    //synchronized for safety
    public static final Map<Player,TeleportTask> runningTasks = Collections.synchronizedMap(new HashMap<>());

    final Player player;
    final TelepadData destination;
    final Double cost;
    protected boolean sneaked = false;

    public TeleportTask(Player player, TelepadData destination, Double cost) throws TeleportRunningException {
        this.player = player;
        this.destination = destination;
        this.cost = cost;
        if(runningTasks.containsKey(player)) {
            throw new TeleportRunningException("Teleport for " + player.getName() + " all ready running.");
        }
        runningTasks.put(this.player, this);
    }

    /**
     * Teleports the player if they have not cancelled previously.
     * Synchronzied to prevent spamming of sneak causing double charging
     */
    public synchronized void sneaked() {
        if(sneaked){
            return;
        }
        this.sneaked = true;
        Optional<Location> loc = destination.safeTeleport();
        if(!loc.isPresent()) {
            player.sendMessage(ChatColor.RED+"No safe location found around destination telepad!"+ChatColor.RESET);
            runningTasks.remove(this.player);
            return;
        }
        if(this.cost > 0) {
            EconomyResponse resp = BSTelepad.getEconomy().withdrawPlayer(this.player, cost);
            if(resp.transactionSuccess()) {
                this.player.teleport(loc.get());
            } else {
                this.player.sendMessage(ChatColor.RED+resp.errorMessage+ChatColor.RESET);
            }
        } else {
            this.player.teleport(loc.get());
        }
    }

    @Override
    public void run() {
        runningTasks.remove(this.player);
       if(!this.sneaked) {
            player.sendTitle("","Teleport cancelled. You did not sneak in time.", 5, 40, 5);
            return;
       } 
       //did the thing
    }

}