package com.gitlab.blockstack;

import com.gitlab.blockstack.bstelepad.BSTelepad;
import com.gitlab.blockstack.bstelepad.listeners.TelepadEntity;
import org.bukkit.plugin.Plugin;

import de.Ste3et_C0st.FurnitureLib.Crafting.Project;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.FurniturePlugin;
import de.Ste3et_C0st.FurnitureLib.main.ObjectID;
import de.Ste3et_C0st.FurnitureLib.main.Type.CenterType;

public class TelepadFurnitureHook extends FurniturePlugin {

  public TelepadFurnitureHook(Plugin pluginInstance) {
    super(pluginInstance);
  }

  @Override
  public void applyPluginFunctions() {
    FurnitureLib.getInstance().getFurnitureManager().getProjects().stream()
        .filter(pro -> pro.getPlugin().getName().equals(getPlugin().getDescription().getName()))
        .forEach(Project::applyFunction);

  }

  @Override
  public void onFurnitureLateSpawn(ObjectID id) {
    //Probably unnecessary now
    // Hook the Furniture to the class
    /*
    if (id.getProject() == null)
      return;
    if (id.getProjectOBJ().getName() == null)
      return;
    if (id.getSQLAction().equals(SQLAction.REMOVE))
      return;
    switch (id.getProjectOBJ().getName()) {
    case "Telepad":
    case "telepad": {
      TelepadData data = BSTelepad.self().getTelepads().get(id.getID());
      if (data == null) {
        data = new TelepadData(id.getID(), id.getStartLocation(), id.getPlayerList().stream().findFirst().get().getUniqueId());
      }
      new TelepadEntity(id, data);
      BSTelepad.self().updateTelepad(data);
      break;
    }
    default:
      break;
    }
    */
  }

  @Override
  public void registerProjects() {
    new Project("Telepad", BSTelepad.self(), getResource("Models/Telepad.dModel"), TelepadEntity.class).setSize(1, 2, 1, CenterType.CENTER)
        .setEditorProject(false);
  }
}